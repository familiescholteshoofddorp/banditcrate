package net.scholtes.banditcrate;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.TileEntity;
import net.minecraft.server.v1_8_R3.TileEntityChest;
import net.scholtes.banditcrate.Events.CrateSpawnEvent;

public class CrateAPI {

	public static HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();
	public static Map<String, Hologram> holos;

	public static void spawnCrate(int tier, Integer min, Integer max, Player p, Block b, String holoname, BlockBreakEvent e) {
		mineCrateSpawn(p, b, (float) Main.getInstance().getConfig().getDouble("settings.tier" + tier + ".chance"),
				(float) Main.getInstance().getConfig().getDouble("settings.tier" + tier + ".donorchance"), e,
				Main.getInstance().getConfig().getInt("settings.tier" + tier + ".cooldown"), tier, holoname, min, max);
	}
	
	public static void mineCrateSpawn(Player p, Block b, Float chance, Float donorchance, BlockBreakEvent e,
			Integer cooldownTime, int tier, String holoname, Integer min, Integer max) {
		if (b.getType().equals(Material.CHEST)) {
			Chest chest = (Chest) b.getState();
			String name = chest.getInventory().getName();
			if (name != null
					&& name.equals(ChatColor.translateAlternateColorCodes('&', "&ac&br&ca&dt&ee &fn&1a&2m&3e"))) {
				p.sendMessage("no");
				e.setCancelled(true);
			}
		}
		if (!e.isCancelled()) {
			Random r = new Random();
			float chance1 = r.nextFloat();
			float chance2 = 0;
			if (p.hasPermission("banditcrate.donor")) {
				chance2 = chance / 100;
			} else {
				chance2 = donorchance / 100;
			}
			if (chance1 <= chance2) {
				if (p.getWorld().getName().equals("world")) {
					if (cooldown.containsKey(p.getUniqueId())
							&& cooldown.get(p.getUniqueId()) > System.currentTimeMillis()) {
						return;
					} else {
						cooldown.put(p.getUniqueId(), System.currentTimeMillis() + (cooldownTime * 1000));
						File data = new File("plugins/BanditCrate/", "chests.yml");
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
						Location loc = b.getLocation();
						BigDecimal rawX = new BigDecimal(loc.getX());
						BigDecimal holox = rawX.setScale(0, RoundingMode.DOWN);
						BigDecimal rawY = new BigDecimal(loc.getY());
						BigDecimal holoy = rawY.setScale(0, RoundingMode.DOWN);
						BigDecimal rawZ = new BigDecimal(loc.getZ());
						BigDecimal holoz = rawZ.setScale(0, RoundingMode.DOWN);
						World holow = loc.getWorld();
						Location hololoc = new Location(holow, holox.doubleValue() + 0.5, holoy.doubleValue() + 2.5,
								holoz.doubleValue() + 0.5);
						Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
							@SuppressWarnings({ "rawtypes", "unchecked" })
							public void run() {
								CrateSpawnEvent event = new CrateSpawnEvent();
								Bukkit.getServer().getPluginManager().callEvent(event);
								loc.getBlock().setType(Material.CHEST);
								Hologram hologram = HologramsAPI.createHologram(Main.getInstance(), hololoc);
								hologram.appendTextLine(ChatColor.translateAlternateColorCodes('&', holoname.replace("%player%", p.getName())));
								hologram.appendTextLine(
										ChatColor.translateAlternateColorCodes('&', "&8(&aRight-click to open&8)"));
								hologram.insertItemLine(0, new ItemStack(Material.NETHER_STAR));
								if (holos == null) {
									holos = new HashMap();
								}
								holos.put(p.getUniqueId().toString(), hologram);
								p.playSound(p.getLocation(), Sound.ENDERDRAGON_GROWL, 3.0F, 0.533F);
								p.playSound(p.getLocation(), Sound.AMBIENCE_THUNDER, 3.0F, 0.533F);
								if (Main.getInstance().getConfig().getString("settings.foundcrate") != null && !Main
										.getInstance().getConfig().getString("settings.foundcrate").equals("")) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											Main.getInstance().getConfig().getString("settings.foundcrate")
													.replace("%prefix%",
															Main.getInstance().getConfig().getString("settings.prefix"))
													.replace("%player%", p.getName().toString())));
								}
								if (data.exists()) {
									if (cfg.getString("locations." + loc) == null) {
										cfg.set("locations." + p.getUniqueId() + ".x", loc.getX());
										cfg.set("locations." + p.getUniqueId() + ".y", loc.getY());
										cfg.set("locations." + p.getUniqueId() + ".z", loc.getZ());
										cfg.set("locations." + p.getUniqueId() + ".world", loc.getWorld().getName());
										cfg.set("locations." + p.getUniqueId() + ".tier", tier);
										cfg.set("locations." + p.getUniqueId() + ".min", min);
										cfg.set("locations." + p.getUniqueId() + ".max", max);
										try {
											cfg.save(data);
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									}
								} else if (!data.exists()) {
									cfg.set("locations." + p.getUniqueId() + ".x", loc.getX());
									cfg.set("locations." + p.getUniqueId() + ".y", loc.getY());
									cfg.set("locations." + p.getUniqueId() + ".z", loc.getZ());
									cfg.set("locations." + p.getUniqueId() + ".world", loc.getWorld().getName());
									cfg.set("locations." + p.getUniqueId() + ".tier", tier);
									cfg.set("locations." + p.getUniqueId() + ".min", min);
									cfg.set("locations." + p.getUniqueId() + ".max", max);
									try {
										cfg.save(data);
									} catch (IOException e1) {
										e1.printStackTrace();
									}
								}
								setName(b, ChatColor.translateAlternateColorCodes('&', "&ac&br&ca&dt&ee &fn&1a&2m&3e"));
							}
						}, 5L);
						Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
							public void run() {
								if (cfg.getString("locations." + p.getUniqueId()) != null) {
									loc.getBlock().setType(Material.AIR);
									((Hologram) holos.get(p.getUniqueId().toString())).delete();
									cfg.set("locations." + p.getUniqueId(), null);
									try {
										cfg.save(data);
									} catch (IOException e1) {
										e1.printStackTrace();
									}

								}
							}
						}, 200L);
					}
				}

			}
		}
	}

	public static void setName(Block block, String name) {
		if (block.getType() != Material.CHEST) {
			// Not a chest
			return;
		}

		// Get the NMS World
		World world = Bukkit.getWorld("world");
		net.minecraft.server.v1_8_R3.World nmsWorld = ((CraftWorld) world).getHandle();
		// Get the tile entity
		TileEntity te = nmsWorld.getTileEntity(new BlockPosition(block.getLocation().getBlockX(),
				block.getLocation().getBlockY(), block.getLocation().getBlockZ()));
		// Make sure it's a TileEntityChest before using it
		if (!(te instanceof TileEntityChest)) {
			// Not a chest :o!
			return;
		}
		((TileEntityChest) te).a(name);
	}

	public static Integer getMin(Integer tier) {
		return Main.getInstance().getConfig().getInt("settings.tier" + tier + ".min");
	}

	public static Integer getMax(Integer tier) {
		return Main.getInstance().getConfig().getInt("settings.tier" + tier + ".max");
	}
	
	public static boolean isSpawned(Player p) {
		File data = new File("plugins/BanditCrate/", "chests.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		if (cfg.getString("locations." + p.getUniqueId()) != null) {
			return true;
		}
		return false;
	}

}
