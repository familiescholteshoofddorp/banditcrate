package net.scholtes.banditcrate;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.scholtes.banditcrate.Main;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("banditcrate")) {
			Player p = (Player) sender;
			if (p.hasPermission("banditcrate.reload")) {
				Bukkit.getPluginManager().getPlugin("BanditCrate").reloadConfig();
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("settings.prefix")
								+ " &aReloaded the config successfully"));
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("settings.prefix")
								+ " &cYou don't have permission to do this!"));
			}
		}

		if (cmd.getName().equalsIgnoreCase("setdonorchance")) {
			Player p = (Player) sender;
			if (p.hasPermission("banditcrate.chance")) {
				if (args.length >= 2) {
					if (isInt(args[0])) {
						if (isDouble(args[1])) {
							Main.getInstance().getConfig().set("settings.tier" + Integer.parseInt(args[0]) + ".chance",
									Double.parseDouble(args[1]));
							Main.getInstance().saveConfig();
							Main.getInstance().reloadConfig();
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									Main.getInstance().getConfig().getString("settings.prefix")
											+ " &aSet the chance for Tier " + args[0] + " crate to: &2" + args[1]));
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									Main.getInstance().getConfig().getString("settings.prefix")
											+ " &cPlease provide a chance!"));
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("settings.prefix")
										+ " &cPlease provide a tier number!"));
					}
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("settings.prefix")
									+ " &cPlease provide a tier and a donor chance (/setchance <tier> <chance>!"));
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("settings.prefix")
								+ " &cYou don't have permission to do this!"));
			}
		}

		if (cmd.getName().equalsIgnoreCase("setdonorchance")) {
			Player p = (Player) sender;
			if (p.hasPermission("banditcrate.chance")) {
				if (args.length >= 2) {
					if (isInt(args[0])) {
						if (isDouble(args[1])) {
							Main.getInstance().getConfig().set(
									"settings.tier" + Integer.parseInt(args[0]) + ".donorchance",
									Double.parseDouble(args[1]));
							Main.getInstance().saveConfig();
							Main.getInstance().reloadConfig();
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									Main.getInstance().getConfig().getString("settings.prefix")
											+ " &aSet the donor chance for Tier " + args[0] + " crate to: &2"
											+ args[1]));
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									Main.getInstance().getConfig().getString("settings.prefix")
											+ " &cPlease provide a donor chance!"));
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								Main.getInstance().getConfig().getString("settings.prefix")
										+ " &cPlease provide a tier number!"));
					}
				} else {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							Main.getInstance().getConfig().getString("settings.prefix")
									+ " &cPlease provide a tier and a donor chance (/setdonorchance <tier> <chance>!"));
				}
			} else {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						Main.getInstance().getConfig().getString("settings.prefix")
								+ " &cYou don't have permission to do this!"));
			}
		}

		return false;
	}

	boolean isDouble(String s) {
		try {
			Double.parseDouble(s);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

	boolean isInt(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

}
