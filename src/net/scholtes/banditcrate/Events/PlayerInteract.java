package net.scholtes.banditcrate.Events;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.gmail.filoghost.holographicdisplays.api.Hologram;

import net.scholtes.banditcrate.CrateAPI;
import net.scholtes.banditcrate.Main;

public class PlayerInteract implements Listener {

	@EventHandler
	public void onClick(PlayerInteractEvent e) {

		if (e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			Player p = e.getPlayer();

			World w = p.getWorld();
			Location loc = e.getClickedBlock().getLocation();

			Block b = w.getBlockAt(loc);
			if (b.getType() != Material.CHEST)
				return;

			Chest chest = (Chest) b.getState();
			String name = chest.getInventory().getName();
			if (e.getClickedBlock().getType().equals(Material.CHEST)) {
				if (name != null
						&& name.equals(ChatColor.translateAlternateColorCodes('&', "&ac&br&ca&dt&ee &fn&1a&2m&3e"))) {
					File data = new File("plugins/BanditCrate/", "chests.yml");
					FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
					e.setCancelled(true);
					Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
						public void run() {
							if (cfg.getString("locations." + p.getUniqueId()) != null) {
								double x = cfg.getDouble("locations." + p.getUniqueId() + ".x");
								double y = cfg.getDouble("locations." + p.getUniqueId() + ".y");
								double z = cfg.getDouble("locations." + p.getUniqueId() + ".z");
								String bw = cfg.getString("locations." + p.getUniqueId() + ".world");
								World w = Bukkit.getWorld(bw);
								if (x == loc.getX() && y == loc.getY() && z == loc.getZ() && w == p.getWorld()) {
									CrateOpenEvent event = new CrateOpenEvent();
									Bukkit.getServer().getPluginManager().callEvent(event);
									b.setType(Material.AIR);
									((Hologram) CrateAPI.holos.get(p.getUniqueId().toString())).delete();
									p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
									p.playSound(p.getLocation(), Sound.FIREWORK_LARGE_BLAST2, 3.0F, 0.533F);
									if (Main.getInstance().getConfig().getString("settings.foundcrate") != null && !Main
											.getInstance().getConfig().getString("settings.openedcrate").equals("")) {
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												Main.getInstance().getConfig().getString("settings.openedcrate")
														.replace("%prefix%",
																Main.getInstance().getConfig()
																		.getString("settings.prefix"))
														.replace("%player%", p.getName().toString())));
									}
									int tier = cfg.getInt("locations." + p.getUniqueId() + ".tier");
									List<String> cmds = Main.getInstance().getConfig()
											.getStringList("settings.tier" + tier + ".commands");
									Collections.shuffle(cmds);
									int min = cfg.getInt("locations." + p.getUniqueId() + ".min");
									int max = cfg.getInt("locations." + p.getUniqueId() + ".max");
									if (max == 0 || min == 0) {
										min = Main.getInstance().getConfig().getInt("settings.tier" + tier + ".min");
										max = Main.getInstance().getConfig().getInt("settings.tier" + tier + ".max");
										int random = ThreadLocalRandom.current().nextInt(min, max + 1);
										for (int i = 0; i <= random - 1; i++) {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
													cmds.get(i).replace("%player%", p.getName().toString()));
										}
										if (cfg.getString("locations." + p.getUniqueId()) != null) {
											cfg.set("locations." + p.getUniqueId(), null);
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									} else {
										int random = ThreadLocalRandom.current().nextInt(min, max + 1);
										for (int i = 0; i <= random - 1; i++) {
											Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
													cmds.get(i).replace("%player%", p.getName().toString()));
										}
										if (cfg.getString("locations." + p.getUniqueId()) != null) {
											cfg.set("locations." + p.getUniqueId(), null);
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									}
								}
							}

						}
					}, 1L);
				}

			}
		}

	}

}
