package net.scholtes.banditcrate.Events;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class BlockBreak implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityExplode(EntityExplodeEvent e) {
		if (e.getEntity() instanceof TNTPrimed) {
			for (Block block : e.blockList()) {
				if (block.getType().equals(Material.CHEST)) {
					Chest chest = (Chest) block.getState();
					String name = chest.getInventory().getName();
					if (name != null && name
							.equals(ChatColor.translateAlternateColorCodes('&', "&ac&br&ca&dt&ee &fn&1a&2m&3e"))) {
						e.setCancelled(true);
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onExplode(BlockExplodeEvent e) {
		for (Block block : e.blockList()) {
			if (block.getType().equals(Material.CHEST)) {
				Chest chest = (Chest) block.getState();
				String name = chest.getInventory().getName();
				if (name != null
						&& name.equals(ChatColor.translateAlternateColorCodes('&', "&ac&br&ca&dt&ee &fn&1a&2m&3e"))) {
					e.setCancelled(true);
				}
			}
		}

	}
	
	
}
