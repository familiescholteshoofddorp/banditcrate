package net.scholtes.banditcrate;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import net.scholtes.banditcrate.Events.BlockBreak;
import net.scholtes.banditcrate.Events.PlayerInteract;

public class Main extends JavaPlugin {

	private static Main instance;
	@SuppressWarnings("unused")
	private boolean useHolographicDisplays;

	public void onEnable() {

		instance = this;

		loadConfig();
		
		File data = new File("plugins/BanditCrate/", "chests.yml");
		data.delete();
		
		getCommand("banditcrate").setExecutor(new Commands());
		getCommand("setchance").setExecutor(new Commands());
		getCommand("setdonorchance").setExecutor(new Commands());
		
		getServer().getPluginManager().registerEvents(new BlockBreak(), this);
		getServer().getPluginManager().registerEvents(new PlayerInteract(), this);
		useHolographicDisplays = Bukkit.getPluginManager().isPluginEnabled("HolographicDisplays");

	}

	public void onDisable() {

	}

	public static Main getInstance() {
		return instance;
	}
	
	public void loadConfig() {
		getConfig().options().copyDefaults(true);
		saveConfig();
	}

}
